#!/bin/bash

# update_in_progress = false
source "~/auto_restart_duniter/variables.sh"
# echo $update_in_progress

if [ "`ps -e|grep duniter|grep -v grep`" == "" ];then
	if [ $update_in_progress != "true" ];then
		echo "Restarting Duniter at `date`..." >>  ~/auto_restart_duniter/logs.txt
		~/duniter/bin/duniter start
	else
		echo "`date` : Update in progress" >>  ~/auto_restart_duniter/logs.txt
	fi
fi
# echo "`date '+%d-%m-%Y %H:%M'` : running" >>  ~/auto_restart_duniter/logs.txt
