# Objectif
Le but de ce script c'est de limiter le temps d'arrêt d'un noeud à moins d'une minute.
Il arrive que certain noeuds se coupent sans raison identifiée.
Il est donc nécessaire de les redémarrer.
Ce serait automatise ça.

# Installation
Cloner le dossier dans le home de votre linux

Ajouter le Cron suivant avec la commande `crontab -e` (ie. copier la ligne en bas du fichier qui va s'ouvrir)

`* * * * * bash ~/auto_restart_duniter/auto_start.sh`

Ce script est prévu pour fonctionner avec des alias :

`alias dustart="echo \"update_in_progress=\\\"true\\\"\" > ~/auto_restart_duniter/variables.sh"`

`alias dustop="echo \"update_in_progress=\\\"false\\\"\" > ~/auto_restart_duniter/variables.sh"`

`alias duniter="~/duniter/bin/duniter"`

`alias dkey="duniter wizard key"`

`alias dnet="duniter wizard network"`

`alias dstart="duniter start"`

`alias dstop="duniter stop"`

`alias dlogs="duniter logs"`

`alias dstatus="duniter status"`

`alias drestart="duniter restart"`

`alias dsssnl="dustart && dstop && duniter sync duniter.normandie-libre.fr && dstart && dustop"`

`alias dsssg1="dustart && dstop && duniter sync g1.duniter.fr && dstart && dustop"`

`alias dsssmoul="dustart && stop && duniter sync duniter.moul.re && dstart && dustop"`

`alias dsssjytou="dustart && dstop && duniter sync g1.jitou.fr && dstart && dustop"`

Vous pouvez ajouter ces alias dans votre .bashrc ou .zshrc.

Lorsque que dustart est lancé, l'automatisation du redémarrage de duniter est stoppé.
Lorsque que dustop est lancé, l'automatisation du redémarrage de duniter est active.
